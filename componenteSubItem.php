<div class="col-md-12" id="linea_<?= (int)$_GET['index'] ?>_<?= (int)$_GET['index2'] ?>" style="height: 2px; background: #3097d1; margin: 11px; width: 97%;"></div>

<div class="row subitem subitem_<?= (int)$_GET['index'] ?> subitem_<?= (int)$_GET['index'] ?>_<?= (int)$_GET['index2'] ?>" id="subitem_<?= (int)$_GET['index'] ?>_<?= (int)$_GET['index2'] ?>">
	<div class="col-md-4">
		<label>Item</label> <br>
        <label class="subLabelItem"><?= (int)$_GET['index'] + 1?>.<?= (int)$_GET['index2'] + 1?></label>
        <button type="button" class="btn btn-danger" onclick="javascript:eliminaSubItem(<?= (int)$_GET['index'] ?>, <?= (int)$_GET['index2'] ?>);"> X </button>
	</div>
	<div class="col-md-4">
		<label>Descripción</label>
        <input name="item[<?= (int)$_GET['index'] ?>][subitem][<?= (int)$_GET['index2'] ?>][descripcion]" type="text" class="form-control descripcion" placeholder="Descripción">
	</div>
	<div class="col-md-4">
		<label>Unidad</label>
        <input name="item[<?= (int)$_GET['index'] ?>][subitem][<?= (int)$_GET['index2'] ?>][unidad]" type="text" class="form-control unidad" placeholder="UN">
	</div>
	<div class="col-md-4">
		<label>Cantidad</label>
		<input name="item[<?= (int)$_GET['index'] ?>][subitem][<?= (int)$_GET['index2'] ?>][cantidad]" type="number" class="form-control cantidad" placeholder="#">
	</div>
	<div class="col-md-4">
		<label>Valor</label>
		<input name="item[<?= (int)$_GET['index'] ?>][subitem][<?= (int)$_GET['index2'] ?>][valor]" type="number" class="form-control valor" placeholder="Valor">
	</div>
	<div class="col-md-4">
		<label>Sub - total</label>
        <input name="item[<?= (int)$_GET['index'] ?>][subitem][<?= (int)$_GET['index2'] ?>][subTotalCampo]" type="number" class="form-control subTotalCampo" placeholder="total" readonly="true" >
	</div>	
</div>
