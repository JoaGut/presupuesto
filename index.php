<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Presupuesto</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="panel panel-primary">
	                <div class="panel-heading">
	                    <div class="row">
	                        <div class="col-md-8">
	                            <h4><strong>Presupuestos - Nuevo</strong></h4>
	                        </div>
	                        <div class="col-md-4">
	                            <button class="btn btn-small btn-default"><strong>Volver</strong></button>
	                        </div>
	                    </div>
	                </div>

	                <div class="panel-body">

	                    <?php include 'assets/base/datos.php'; ?>
	                    
	                    <hr>
	                    
	                    <div class="row bg-info">
	                        <div class="col-md-12">
	                            <h4><strong>Cliente</strong></h4>
	                        </div>
	                    </div>
	                    
	                    <hr>

	                    <form class="form-horizontal">
	                        <?php include 'assets/base/datosCliente.php'; ?>
	                        <hr>
	                        
	                        <div class="row bg-info">
	                            <div class="col-md-8">
	                                <h4><strong>Descripción</strong></h4>
	                            </div>

	                            <div class="col-md-4">
	                                <button class="btn btn-default" type="button" onclick="javascript:agregarItem();"><strong>Agregar</strong></button>
	                            </div>
	                        </div>
	                        
	                        <div id="description">
	                        </div>
	                        
	                        <hr>

	                        <div class="form-group">
	                            <div class="col-sm-offset-2 col-sm-10">
	                                <button type="submit" class="btn btn-default">Sign in</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
    		agregarItem();	
    	});
    </script>
</body>
</html>
