
const app = new Vue({
    el: '#app',
    data: {
        campo: {
            descripcion: '',
            subcampos:[{
                descripcion: '',
                unidad: '',
                cantidad: '',
                valor: '',
                subTotalCampo: 0
            }]
        },
        subcampos:{
            descripcion: '',
            unidad: '',
            cantidad: '',
            valor: '',
            subTotalCampo: 0
        },
        campos: [],
        totalNeto: 0,
    }, mounted: function () {
        this.campos = JSON.parse(this.$el.dataset.campos);


    }, methods: {
        totalLinea:function(index,index2) {

            this.campos[index].subcampos[index2].subTotalCampo = Number(this.campos[index].subcampos[index2].cantidad)*Number(this.campos[index].subcampos[index2].valor);
            return this.campos[index].subcampos[index2].subTotalCampo;
        },
        addCampoNuevo: function () {
          console.log(this.campo);
            this.campos.push(Vue.util.extend({}, this.campo))
        },
        addSubCampoNuevo: function (index) {
            this.campos[index].subcampos.push(Vue.util.extend({}, this.subcampos))
        },
        borrarCampo: function (index) {
            Vue.delete(this.campos, index);
        },
        borrarSubCampo: function (index,index2) {
            Vue.delete(this.campos[index].subcampos, index2);
        },
        totalSinIva: function () {
            var auxCampo2 = 0;
            for (var i = 0; i < this.campos.length; i++) {
                for (var x = 0; x < this.campos[i].subcampos.length; x++) {
                    if (isNaN(this.campos[i].subcampos[x].subTotalCampo)) { this.campos[i].subcampos[x].subTotalCampo = 0;}
                    auxCampo2 = auxCampo2 + Number(this.campos[i].subcampos[x].subTotalCampo);
                }
            }
            return auxCampo2;
        },
        totalConIva: function () {
            var auxCampo = 0;
            for (var i = 0; i < this.campos.length; i++) {
                for (var x = 0; x < this.campos[i].subcampos.length; x++) {
                    auxCampo = auxCampo + Number(this.campos[i].subcampos[x].subTotalCampo);
                }
            }

            totalConIva = auxCampo * 1.19;
            return totalConIva.toFixed();

        }, 
        valorIva: function(){
            var totalConIva = this.totalConIva();
            var subTotal = this.totalSinIva();

            return totalConIva - subTotal;
        },
        sumbitForm: function () {
          /*
           * You can remove or replace the "submitForm" method.
           * Remove: if you handle form sumission on server side.
           * Replace: for example you need an AJAX submission.
           */
          console.info('<< Form Submitted >>')
          console.info('Vue.js campos object:', this.campos)
          window.testSumbit()
        }
    }
})

