<hr class="hr_<?= (int)$_GET['index'] ?>">

<div class="row item" id="item_<?= $_GET['index'] ?>">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-4">
				<label>Item</label> <br>
	        	<label class="labelItem"><?= (int)$_GET['index'] + 1?></label>
			</div>
			<div class="col-md-4">
				<label>Descripción</label>
	        	<input name="item[<?= (int)$_GET['index'] ?>][descripcion]" type="text" class="form-control descripcion" placeholder="Descripción">
			</div>
			<div class="col-md-4 btn-group">
				<label>&nbsp;</label>
	            <a type="button" class="btn btn-success agregarSubItem" onclick="javascript:agregarSubItem(<?= (int)$_GET['index'] ?>,0);">
	                Agregar Sub item
	            </a>
	            <a type="button" class="btn btn-danger eliminarItem" onclick="javascript:eliminarItem(<?= (int)$_GET['index'] ?>,0);">
	                Eliminar item 
	            </a>
			</div>
		</div>
	</div>

	<div class="col-md-12" id="linea_<?= (int)$_GET['index'] ?>_0" style="height: 2px; background: #3097d1; margin: 11px; width: 97%;"></div>

	<div class="col-md-12 subItemCampos" id="subItemCampos_<?= (int)$_GET['index'] ?>">
		<div class="row subitem subitem_<?= (int)$_GET['index'] ?> subitem_<?= (int)$_GET['index'] ?>_0" id="subitem_<?= (int)$_GET['index'] ?>_0">
			<div class="col-md-4">
				<label>Item</label> <br>
	            <label class="subLabelItem"><?= (int)$_GET['index'] + 1?>.1</label>
	            <button type="button" class="btn btn-danger" onclick="javascript:eliminaSubItem(<?= (int)$_GET['index'] ?>, 0);"> X </button>
			</div>
			<div class="col-md-4">
				<label>Descripción</label>
	            <input name="item[<?= (int)$_GET['index'] ?>][subitem][0][descripcion]" type="text" class="form-control descripcion" placeholder="Descripción">
			</div>
			<div class="col-md-4">
				<label>Unidad</label>
	            <input name="item[<?= (int)$_GET['index'] ?>][subitem][0][unidad]" type="text" class="form-control unidad" placeholder="UN">
			</div>
			<div class="col-md-4">
				<label>Cantidad</label>
        		<input name="item[<?= (int)$_GET['index'] ?>][subitem][0][cantidad]" type="number" class="form-control cantidad" placeholder="#">
			</div>
			<div class="col-md-4">
				<label>Valor</label>
        		<input name="item[<?= (int)$_GET['index'] ?>][subitem][0][valor]" type="number" class="form-control valor" placeholder="Valor">
			</div>
			<div class="col-md-4">
				<label>Sub - total</label>
	            <input name="item[<?= (int)$_GET['index'] ?>][subitem][0][subTotalCampo]" type="number" class="form-control subTotal" placeholder="total" readonly="true" >
			</div>	
		</div>
	</div>
</div>
