<div class="row">
    <div class="col-md-4">
        <strong>CONTRATISTA:</strong>
        <label>Juan Cabello Araya</label>
    </div>
    <div class="col-md-4">
        <strong>Dirección:</strong>
        <label>Mar del Norte 498 - Maipú</label>
    </div>
    <div class="col-md-4">
        <strong>RUT:</strong>
        <label>7.687.281-3</label>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <strong>TELEFONO:</strong>
        <label>+569 6190 5136</label>
    </div>
    <div class="col-md-4">
        <strong>FECHA EMISIÓN:</strong>
        <label><?php date('d/m/Y') ?></label>
    </div>
    <div class="col-md-4">
        <strong>FOLIO:</strong>
        <label>1</label>
    </div>
</div>