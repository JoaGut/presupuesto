function agregarItem() {
    count = $('.item').length;
    $.get('componenteItem.php', { index : count }, function(resp) {
        $('#description').append(resp);
    });
}

function agregarSubItem(index,subItem) {
    count = $('.subitem_'+index).length;
    $.get('componenteSubItem.php', { index : index , index2 : count }, function(resp) {
        $('#subItemCampos_'+index).append(resp);
    });

}

function eliminarItem(item,subItem) {
    $('#item_'+item+', .hr_'+item ).remove();

    $('.item_'+item).each(function(indice, elemento) {
        auxSubitem = indice+1;
        auxItem = item+1;
        $(elemento).find('.labelItem').html(auxSubitem);
        $(elemento).find('.agregarItem').find('button').attr('onclick', 'javascript:agregarSubItem('+indice+','+subItem+');');
        $(elemento).find('.eliminarItem').find('button').attr('onclick', 'javascript:eliminarItem('+indice+','+subItem+');');
        eliminaSubItem(item,subItem);
    });
} 

function eliminaSubItem(item,subItem) {
    $('#subitem_'+item +'_'+subItem+', #linea_'+item +'_'+subItem).remove();

    $('.subitem_'+item).each(function(indice, elemento) {
        auxSubitem = indice+1;
        auxItem = item+1;
        $(elemento).find('.subLabelItem').html(auxItem+ '.'+auxSubitem);
        $(elemento).find('button').find('button').attr('onclick', 'javascript:eliminaSubItem('+item+','+indice+');');
        $(elemento).find('.descripcion').attr('name', 'item['+item+'][subitem]['+indice+'][descripcion]' );
        $(elemento).find('.unidad').attr('name', 'item['+item+'][subitem]['+indice+'][unidad]' );
        $(elemento).find('.cantidad').attr('name', 'item['+item+'][subitem]['+indice+'][cantidad]' );
        $(elemento).find('.valor').attr('name', 'item['+item+'][subitem]['+indice+'][valor]' );
        $(elemento).find('.subTotalCampo').attr('name', 'item['+item+'][subitem]['+indice+'][subTotalCampo]' );
    });
} 


